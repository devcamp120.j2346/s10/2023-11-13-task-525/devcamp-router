import { Route, Routes } from "react-router-dom"
import routes from "../routes"

const Body = () => {
    return (
        <> 
            <Routes>
                {
                    routes.map((value, index) => {
                        return <Route path={value.path} element={value.element} key={index} />
                    })
                }
            </Routes>
        </>
    )
}

export default Body