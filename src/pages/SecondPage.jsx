import { useEffect } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom"

const SecondPage = () => {
    //load tham số trên đường dẫn url
    const {orderId, typeId} = useParams();
    console.log("orderId: " + orderId);
    console.log("typeId: " + typeId);

    //load tham số query string: http://localhost:3000/secondpage/12/5?name=abc&age=12
    const [query] = useSearchParams();
    console.log(query);
    console.log(query.get("name"));
    console.log(query.get("age"));

    const navigate = useNavigate();
    
    useEffect(() => {
        if (typeId == 3) {
            navigate("/thirdpage");
        }
    }, [])
    
    return (
        <> 
            <p>Second Page</p>
        </>
    )
}

export default SecondPage