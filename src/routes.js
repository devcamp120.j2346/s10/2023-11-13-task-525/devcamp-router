import FirstPage from "./pages/FirstPage";
import HomePage from "./pages/HomePage";
import NotFound from "./pages/NotFound";
import SecondPage from "./pages/SecondPage";
import ThirdPage from "./pages/ThirdPage";

const routes = [
    { path:"/", element:<HomePage/> }, 
    { label:"First Page", path:"/firstpage", element:<FirstPage/> }, 
    { label:"Second Page", path:"/secondpage", element:<SecondPage/> }, 
    { label:"Second Page", path:"/secondpage/:orderId", element:<SecondPage/> }, 
    { label:"Second Page", path:"/secondpage/:orderId/:typeId", element:<SecondPage/> }, 
    { label:"Third Page", path:"/thirdpage", element:<ThirdPage/> }, 
    { path:"*", element:<NotFound/> }, 
]

export default routes;